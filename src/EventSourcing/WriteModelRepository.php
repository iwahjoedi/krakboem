<?php

namespace RvaVzw\KrakBoem\EventSourcing;

use RvaVzw\KrakBoem\EventSourcing\Aggregate\Aggregate;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;

/**
 * @template TAggregate of Aggregate
 * @template TId of AggregateRootIdentifier
 */
interface WriteModelRepository
{
    /**
     * @param TId $identifier
     * @return TAggregate
     */
    public function get(AggregateRootIdentifier $identifier): Aggregate;

    /**
     * @param TAggregate $aggregate
     */
    public function save(Aggregate $aggregate): void;

    /**
     * @param TId $identifier
     */
    public function exists(AggregateRootIdentifier $identifier): bool;
}
