<?php

namespace RvaVzw\KrakBoem\EventSourcing\EventStore;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;

/**
 * Event with metadata, to be stored in the event store.
 */
final class StoredEvent
{
    /**
     * @var ?int
     */
    private $id;

    /**
     * @var AggregateRootIdentifier
     */
    private $aggregateRootIdentifier;

    /**
     * @var int
     */
    private $aggregateVersion;

    /**
     * @var string
     */
    private $eventType;

    /**
     * @var array<mixed>
     */
    private $payload = [];

    /**
     * @var DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param array<mixed> $payload
     */
    public function __construct(
        AggregateRootIdentifier $aggregateRootIdentifier,
        int $aggregateVersion,
        string $eventType,
        array $payload,
        DateTimeImmutable $createdAt
    ) {
        $this->aggregateRootIdentifier = $aggregateRootIdentifier;
        $this->aggregateVersion = $aggregateVersion;
        $this->eventType = $eventType;
        $this->payload = $payload;

        $this->createdAt = $createdAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAggregateRootIdentifier(): AggregateRootIdentifier
    {
        return $this->aggregateRootIdentifier;
    }

    public function getAggregateVersion(): int
    {
        return $this->aggregateVersion;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    /**
     * @return array<mixed>
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
