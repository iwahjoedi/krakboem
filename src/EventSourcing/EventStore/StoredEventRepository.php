<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\EventStore;

use Generator;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;

/**
 * A repository to store and retrieve events.
 *
 * You'll have to provide an implementation yourself.
 * (I might provide one in a KrakBoemBundle at some point in the future.)
 */
interface StoredEventRepository
{
    public function save(StoredEvent $storedEvent): void;

    /**
     * @return Generator<StoredEvent>
     */
    public function getAllForAggregate(
        AggregateRootIdentifier $aggregateRootIdentifier
    ): Generator;

    public function hasByAggregateRoot(AggregateRootIdentifier $identifier): bool;

    /**
     * Returns all events; this is used for replay.
     *
     * @return Generator<StoredEvent>
     */
    public function getStream(): Generator;
}
