<?php

namespace RvaVzw\KrakBoem\Infrastructure;

use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

/**
 * Some identifier.
 *
 * FIXME: I don't like these annotations, but I seem to need them as long as I use Symfony's default serialization.
 *
 * @DiscriminatorMap(typeProperty="type", mapping={
 *     "uuid_identifier"="App\Infrastructure\UuidIdentifier"
 * })
 */
interface Identifier
{
    /**
     * @param string $value
     *
     * @return static
     */
    public static function fromString(string $value): self;

    public function toString(): string;
}
