<?php

namespace RvaVzw\KrakBoem\Infrastructure;

use Ramsey\Uuid\Rfc4122\FieldsInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class Uuid5Identifier implements Identifier
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param UuidInterface $uuid
     *
     * @throws IdentifierException
     */
    final private function __construct(UuidInterface $uuid)
    {
        $this->value = $uuid->toString();

        /** @var FieldsInterface $fields */
        $fields = $uuid->getFields();

        if (5 == $fields->getVersion()) {
            return;
        }

        throw new IdentifierException('Uuid v5 expected.');
    }

    abstract protected static function getNamespace(): string;

    /**
     * @param string $value
     *
     * @return static
     *
     * @throws IdentifierException
     */
    public static function fromString(string $value): Identifier
    {
        // Go via Uuid to make sure that we're dealing with a UUID.
        return new static(Uuid::fromString($value));
    }

    public function toString(): string
    {
        return $this->value;
    }

    /**
     * @param string $name
     *
     * @return static
     */
    public static function fromName(string $name): self
    {
        $uuid = Uuid::uuid5(static::getNamespace(), $name);

        /* @noinspection PhpUnhandledExceptionInspection */
        return new static($uuid);
    }
}
