<?php

namespace RvaVzw\KrakBoem\Cqrs\CommandBus;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final class MessengerCommandBus implements CommandBus
{
    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function dispatch(Command ...$commands): void
    {
        foreach ($commands as $command) {
            $this->messageBus->dispatch(
                new Envelope($command)
            );
        }
    }
}
