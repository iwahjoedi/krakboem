<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\Replay;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class ReplayConsoleCommand extends Command
{
    protected static $defaultName = 'eventsourcing:readmodel:replay';
    /** @var Replayer */
    private $replayer;

    public function __construct(Replayer $replayer)
    {
        parent::__construct();
        $this->replayer = $replayer;
    }

    protected function configure(): void
    {
        $this->setDescription('Re-emit all events from the store to the replay bus.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->replayer->replayAll();
        $io->success('Replay completed.');

        return 0;
    }
}
