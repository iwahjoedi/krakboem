<?php

namespace RvaVzw\KrakBoem\EventSourcing\EventBus;

use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;

final class EventStoreMiddleware implements MiddlewareInterface
{
    /**
     * @var EventStore
     */
    private $eventStore;

    public function __construct(EventStore $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        /** @var Event $event */
        $event = $envelope->getMessage();

        $receivedStamp = $envelope->last(ReceivedStamp::class);
        if (empty($receivedStamp)) {
            // Only record the event if the message wasn't received,
            // see #14
            /** @var AggregateVersionStamp $stamp */
            $stamp = $envelope->last(AggregateVersionStamp::class);
            $this->eventStore->save($event, $stamp->getAggregateVersion());
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
