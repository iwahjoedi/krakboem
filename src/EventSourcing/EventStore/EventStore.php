<?php

namespace RvaVzw\KrakBoem\EventSourcing\EventStore;

use Generator;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;

interface EventStore
{
    public function save(Event $event, int $aggregateVersion): void;

    /**
     * @return Generator<Event>
     */
    public function getStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): Generator;

    public function hasStreamForAggregate(AggregateRootIdentifier $identifier): bool;

    /**
     * @return Generator<Event>
     */
    public function getStream(): Generator;
}
