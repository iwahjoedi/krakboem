<?php

namespace RvaVzw\KrakBoem\EventSourcing\Aggregate;

use RvaVzw\KrakBoem\EventSourcing\Event;

abstract class AbstractAggregate implements Aggregate
{
    /**
     * @var AggregateRootIdentifier
     */
    private $identifier;

    /** @var int */
    private $version;

    // This array maps the aggregate version number to the latest applied event.
    /** @var Event[] */
    private $uncommittedEvents;

    /**
     * A final constructor.
     *
     * Initializing the aggregate root is done by the first even, which may
     * differ on aggregate root against the other.
     *
     * @param AggregateRootIdentifier $identifier
     */
    final public function __construct(AggregateRootIdentifier $identifier)
    {
        $this->identifier = $identifier;
        $this->version = 0;
        $this->uncommittedEvents = [];
    }

    public function getIdentifier(): AggregateRootIdentifier
    {
        return $this->identifier;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @return Event[]
     */
    public function getUncommittedEvents(): array
    {
        return $this->uncommittedEvents;
    }

    /**
     * @param iterable<Event> $events
     *
     * @return static
     */
    public static function reconstitute(iterable $events): Aggregate
    {
        $aggregate = null;
        /** @var Event $event */
        foreach ($events as $event) {
            if (null === $aggregate) {
                $aggregate = new static($event->getAggregateRootIdentifier());
            }
            $aggregate->apply($event);
        }

        if (null === $aggregate) {
            throw new \InvalidArgumentException('Cannot reconstitute from empty stream');
        }

        return $aggregate;
    }

    public function apply(Event $event): void
    {
        // FIXME: I don't like this magic.

        /** @noinspection PhpUnhandledExceptionInspection */
        $reflect = new \ReflectionClass($event);
        $applyFunctionName = 'apply'.$reflect->getShortName();

        if (method_exists($this, $applyFunctionName)) {
            $this->$applyFunctionName($event);
        }
        ++$this->version;
    }

    protected function applyAndRecord(Event $event): void
    {
        $this->apply($event);
        $this->uncommittedEvents[$this->version] = $event;
    }

    public function clearUncommittedEvents(): void
    {
        $this->uncommittedEvents = [];
    }
}
