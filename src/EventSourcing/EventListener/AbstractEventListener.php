<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\EventListener;

use RvaVzw\KrakBoem\EventSourcing\Event;

abstract class AbstractEventListener implements EventListener
{
    public function __invoke(Event $event): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflect = new \ReflectionClass($event);
        $applyFunctionName = 'apply'.$reflect->getShortName();

        if (method_exists($this, $applyFunctionName)) {
            $this->$applyFunctionName($event);
        }
    }
}
