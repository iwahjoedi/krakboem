<?php

namespace RvaVzw\KrakBoem\EventSourcing;

use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;

/**
 * A domain event.
 */
interface Event
{
    public function getAggregateRootIdentifier(): AggregateRootIdentifier;
}
