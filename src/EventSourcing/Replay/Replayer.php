<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\Replay;

use Psr\Log\LoggerInterface;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * This class republishes the existing event store to the replay bus.
 */
final class Replayer
{
    /** @var MessageBusInterface */
    private $messageBus;
    /** @var EventStore */
    private $eventStore;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        MessageBusInterface $messageBus,
        EventStore $eventStore,
        LoggerInterface $logger
    ) {
        $this->messageBus = $messageBus;
        $this->eventStore = $eventStore;
        $this->logger = $logger;
    }

    public function replayAll(): void
    {
        foreach ($this->eventStore->getStream() as $event) {
            try {
                $this->messageBus->dispatch(new Envelope($event));
            } catch (HandlerFailedException $ex) {
                $eventClass = get_class($event);
                $this->logger->error(
                    "Exception during replay while handling an event of type {$eventClass}.",
                    [
                        $ex->getPrevious()
                    ]
                );
            }
        }
    }
}
