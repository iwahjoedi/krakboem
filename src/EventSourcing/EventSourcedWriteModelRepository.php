<?php

namespace RvaVzw\KrakBoem\EventSourcing;

use RvaVzw\KrakBoem\EventSourcing\Aggregate\Aggregate;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;

/**
 * @template TAggregate of Aggregate
 * @template TId of AggregateRootIdentifier
 * @implements WriteModelRepository<TAggregate, TId>
 */
abstract class EventSourcedWriteModelRepository implements WriteModelRepository
{
    /**
     * @var EventStore
     */
    private $eventStore;
    /**
     * @var EventBus
     */
    private $eventBus;
    /** @var class-string<TAggregate> */
    private $aggregateType;

    /**
     * EventSourcedWriteModelRepository constructor.
     * @param class-string<TAggregate> $aggregateType
     */
    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        string $aggregateType
    ) {
        $this->eventStore = $eventStore;
        $this->eventBus = $eventBus;
        $this->aggregateType = $aggregateType;
    }

    public function get(AggregateRootIdentifier $identifier): Aggregate
    {
        /** @var callable $reconstituteFunction */
        $reconstituteFunction = [$this->aggregateType, 'reconstitute'];

        return $reconstituteFunction(
            $this->eventStore->getStreamForAggregate($identifier)
        );
    }

    public function save(Aggregate $aggregate): void
    {
        foreach ($aggregate->getUncommittedEvents() as $version => $event) {
            $this->eventBus->publish($event, $version);
        }
        $aggregate->clearUncommittedEvents();
    }

    public function exists(AggregateRootIdentifier $identifier): bool
    {
        return $this->eventStore->hasStreamForAggregate($identifier);
    }
}
