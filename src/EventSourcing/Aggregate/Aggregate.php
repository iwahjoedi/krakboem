<?php

namespace RvaVzw\KrakBoem\EventSourcing\Aggregate;

use RvaVzw\KrakBoem\EventSourcing\Event;

/**
 * Interface for an aggregate.
 *
 * I guess it's a good idea to derive your aggregates from @see AbstractAggregate.
 */
interface Aggregate
{
    public function getIdentifier(): AggregateRootIdentifier;

    public function getVersion(): int;

    /**
     * Uncommitted events.
     *
     * The array maps the aggregate version to the last applied event for that version.
     *
     * @return Event[]
     */
    public function getUncommittedEvents(): array;

    /**
     * @param iterable<Event> $events
     *
     * @return static
     */
    public static function reconstitute(iterable $events): self;

    public function apply(Event $event): void;

    public function clearUncommittedEvents(): void;
}
