<?php

namespace RvaVzw\KrakBoem\Cqrs\CommandBus;

/**
 * Marker interface for a command.
 */
interface Command
{
}
