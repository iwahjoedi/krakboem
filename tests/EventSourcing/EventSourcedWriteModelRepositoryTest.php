<?php

/** @noinspection PhpUnhandledExceptionInspection */

namespace RvaVzw\KrakBoem\Tests\EventSourcing;

use RvaVzw\KrakBoem\EventSourcing\Aggregate\Aggregate;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use RvaVzw\KrakBoem\EventSourcing\EventSourcedWriteModelRepository;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;
use Generator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class EventSourcedWriteModelRepositoryTest extends TestCase
{
    /** @var EventStore */
    private $dummyEventStore;

    /** @var EventBus */
    private $dummyEventBus;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dummyEventStore = new class () implements EventStore {
            public function save(Event $event, int $aggregateVersion): void
            {
            }

            public function getStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): \Generator
            {
                return new Generator();
            }

            public function deleteStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): void
            {
            }

            public function hasStreamForAggregate(AggregateRootIdentifier $identifier): bool
            {
                return false;
            }

            public function getStream(): Generator
            {
                return new Generator();
            }
        };

        $this->dummyEventBus = new class () implements EventBus {
            public function publish(Event $event, int $aggregateVersion): void
            {
            }
        };
    }

    /** @test */
    public function itClearsUncommittedEventsAfterSaving(): void
    {
        $dummyEvent = $this->createMock(Event::class);

        /** @var MockObject $aggregateMock */
        $aggregateMock = $this->createMock(Aggregate::class);
        $aggregateMock->method('getUncommittedEvents')->willReturn([1 => $dummyEvent]);
        $aggregateMock->expects($this->atLeastOnce())
            ->method('clearUncommittedEvents');

        // The mocking framework of PHPUnit doesn't play well with phpstan, so I do this pseudo-conversion.
        /** @var Aggregate $aggregate */
        $aggregate = $aggregateMock;

        $repository = new class ($this->dummyEventStore, $this->dummyEventBus, Aggregate::class) extends EventSourcedWriteModelRepository {
        };
        $repository->save($aggregate);
    }
}
