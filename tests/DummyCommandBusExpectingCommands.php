<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Tests;

use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use PHPUnit\Framework\Assert;

final class DummyCommandBusExpectingCommands implements CommandBus
{
    /** @var Command[] */
    private $expectedCommands;

    /**
     * DummyCommandBusExpectingCommands constructor.
     *
     * @param Command[] $expectedCommands
     */
    public function __construct(array $expectedCommands)
    {
        $this->expectedCommands = $expectedCommands;
    }

    /**
     * @param Command ...$commands
     */
    public function dispatch(Command ...$commands): void
    {
        foreach ($commands as $command) {
            $this->shift($command);
        }
    }

    /**
     * @param Command $command
     */
    private function shift(Command $command): void
    {
        $expectedCommand = array_shift($this->expectedCommands);
        Assert::assertEquals($expectedCommand, $command);
    }
}
