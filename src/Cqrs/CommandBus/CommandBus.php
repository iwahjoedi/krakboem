<?php

namespace RvaVzw\KrakBoem\Cqrs\CommandBus;

interface CommandBus
{
    public function dispatch(Command ...$commands): void;
}
