<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\Replay;

use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventListener\EventListener;

interface Replayable extends EventListener
{
    public function __invoke(Event $event): void;
}
