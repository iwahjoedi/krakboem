<?php

namespace RvaVzw\KrakBoem\EventSourcing\EventBus;

use RvaVzw\KrakBoem\EventSourcing\Event;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final class MessengerEventBus implements EventBus
{
    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * Publishes the event that leads to the given version of the aggregate.
     *
     * @param Event $event
     * @param int   $aggregateVersion
     */
    public function publish(Event $event, int $aggregateVersion): void
    {
        $this->messageBus->dispatch(
            (new Envelope($event))->with(new AggregateVersionStamp($aggregateVersion))
        );
    }
}
