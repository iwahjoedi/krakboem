<?php

namespace RvaVzw\KrakBoem\EventSourcing\EventBus;

use RvaVzw\KrakBoem\EventSourcing\Event;

interface EventBus
{
    /**
     * Publishes the event that leads to the given version of the aggregate.
     *
     * @param Event $event
     * @param int   $aggregateVersion
     */
    public function publish(Event $event, int $aggregateVersion): void;
}
