<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\Infrastructure\Normalizer;

use RvaVzw\KrakBoem\Infrastructure\Identifier;
use RvaVzw\KrakBoem\Infrastructure\IntWrapper;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class IntWrapperNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed              $data
     * @param array<mixed>       $context
     *
     * @return Identifier
     *
     * @throws \ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        if (!class_exists($type)) {
            throw new \InvalidArgumentException();
        }

        $reflectionMethod = new \ReflectionMethod($type, 'fromInteger');

        /** @var Identifier $identifier */
        $identifier = $reflectionMethod->invoke(null, $data);

        return $identifier;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @return bool
     */
    public function supportsDenormalization($data, $type, string $format = null)
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        /** @var string[] $interfaces */
        $interfaces = class_implements($type);

        return in_array(IntWrapper::class, $interfaces);
    }

    /**
     * @param mixed $object
     * @param string|null $format
     * @param array<mixed> $context
     * @return int
     */
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var IntWrapper $intWrapper */
        $intWrapper = $object;

        return $intWrapper->toInteger();
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed  $data   Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof IntWrapper;
    }
}
