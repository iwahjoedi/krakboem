<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\Projector;

use RvaVzw\KrakBoem\EventSourcing\EventListener\AbstractEventListener;
use RvaVzw\KrakBoem\EventSourcing\Replay\Replayable;

abstract class AbstractProjector extends AbstractEventListener implements Replayable
{
}
