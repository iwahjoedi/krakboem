<?php

namespace RvaVzw\KrakBoem\Infrastructure;

/**
 * Marker interface for objects that should be (de)normalized from and to an int.
 */
interface IntWrapper
{
    /**
     * @param int $value
     *
     * @return static
     */
    public static function fromInteger(int $value): self;

    public function toInteger(): int;
}
