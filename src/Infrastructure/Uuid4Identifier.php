<?php

namespace RvaVzw\KrakBoem\Infrastructure;

use Ramsey\Uuid\Uuid;

abstract class Uuid4Identifier implements Identifier
{
    /** @var string */
    private $value;

    /**
     * Public constructor.
     *
     * I'm not sure the constructor should actually be public.
     *
     * @param string $value
     */
    final public function __construct(string $value)
    {
        // Check whether this is a valid UUID.
        if (!Uuid::isValid($value)) {
            throw new \InvalidArgumentException('Invalid UUID');
        }
        $this->value = strtolower($value);
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public static function fromString(string $value): Identifier
    {
        // Go via Uuid to make sure that we're dealing with a UUID.
        return new static($value);
    }

    /**
     * @return static
     */
    public static function create(): self
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $uuid4 = Uuid::uuid4();

        return new static($uuid4->toString());
    }

    public function toString(): string
    {
        return $this->value;
    }
}
