<?php

namespace RvaVzw\KrakBoem\Infrastructure\Normalizer;

use RvaVzw\KrakBoem\Infrastructure\Identifier;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class IdentifierNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param mixed       $object
     * @param array<mixed>       $context
     *
     * @return string
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        /** @var Identifier $identifier */
        $identifier = $object;

        return $identifier->toString();
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed  $data   Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof Identifier;
    }

    /**
     * @param mixed              $data
     * @param array<mixed>       $context
     *
     * @return Identifier
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        if (!class_exists($type)) {
            throw new \InvalidArgumentException();
        }

        $reflectionMethod = new \ReflectionMethod($type, 'fromString');

        /** @var Identifier $identifier */
        $identifier = $reflectionMethod->invoke(null, $data);

        return $identifier;
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed  $data   Data to denormalize from
     * @param string $type   The class to which the data should be denormalized
     * @param string $format The format being deserialized from
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        if ('[]' === substr($type, -2, 2)) {
            return false;
        }

        /** @var string[] $interfaces */
        $interfaces = class_implements($type);

        return in_array(Identifier::class, $interfaces);
    }
}
