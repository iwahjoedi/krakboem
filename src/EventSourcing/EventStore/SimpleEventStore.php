<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoem\EventSourcing\EventStore;

use Generator;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\Aggregate\AggregateRootIdentifier;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use RvaVzw\KrakBoem\EventSourcing\EventStore\Orm\Repository\OrmStoredEventRepository;

final class SimpleEventStore implements EventStore
{
    /**
     * @var StoredEventRepository
     */
    private $eventRepository;
    /**
     * @var NormalizerInterface
     */
    private $normalizer;
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    public function __construct(
        StoredEventRepository $eventRepository,
        NormalizerInterface $normalizer,
        DenormalizerInterface $denormalizer
    ) {
        $this->eventRepository = $eventRepository;
        $this->normalizer = $normalizer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @param Event $event
     * @param int   $aggregateVersion
     */
    public function save(Event $event, int $aggregateVersion): void
    {
        $payload = $this->normalizer->normalize($event);

        if (!is_array($payload)) {
            throw new \InvalidArgumentException('Events should be normalized to arrays.');
        }

        $storedEvent = new StoredEvent(
            $event->getAggregateRootIdentifier(),
            $aggregateVersion,
            get_class($event),
            $payload,
            new \DateTimeImmutable()
        );

        $this->eventRepository->save($storedEvent);
    }

    /**
     * @param AggregateRootIdentifier $aggregateRootIdentifier
     *
     * @return Generator<Event>
     */
    public function getStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): Generator
    {
        /** @var StoredEvent $storedEvent */
        foreach ($this->eventRepository->getAllForAggregate($aggregateRootIdentifier) as $storedEvent) {
            /** @var Event $event */
            $event = $this->denormalizer->denormalize(
                $storedEvent->getPayload(),
                $storedEvent->getEventType()
            );
            yield $storedEvent->getAggregateVersion() => $event;
        }
    }

    public function hasStreamForAggregate(AggregateRootIdentifier $identifier): bool
    {
        return $this->eventRepository->hasByAggregateRoot(
            $identifier
        );
    }

    /**
     * Returns all events; this is used for replay.
     *
     * @return Generator<Event>
     */
    public function getStream(): Generator
    {
        foreach ($this->eventRepository->getStream() as $storedEvent) {
            /** @var Event $event */
            $event = $this->denormalizer->denormalize(
                $storedEvent->getPayload(),
                $storedEvent->getEventType()
            );
            yield $storedEvent->getAggregateVersion() => $event;
        }
    }
}
