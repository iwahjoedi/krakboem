<?php

namespace RvaVzw\KrakBoem\EventSourcing\EventBus;

use Symfony\Component\Messenger\Stamp\StampInterface;

final class AggregateVersionStamp implements StampInterface
{
    /**
     * @var int
     */
    private $aggregateVersion;

    public function __construct(int $aggregateVersion)
    {
        $this->aggregateVersion = $aggregateVersion;
    }

    public function getAggregateVersion(): int
    {
        return $this->aggregateVersion;
    }
}
