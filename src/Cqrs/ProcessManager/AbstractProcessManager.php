<?php

namespace RvaVzw\KrakBoem\Cqrs\ProcessManager;

use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use RvaVzw\KrakBoem\EventSourcing\EventListener\AbstractEventListener;

abstract class AbstractProcessManager extends AbstractEventListener
{
    /**
     * @var CommandBus
     */
    protected $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }
}
