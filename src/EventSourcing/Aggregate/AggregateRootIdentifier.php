<?php

namespace RvaVzw\KrakBoem\EventSourcing\Aggregate;

use RvaVzw\KrakBoem\Infrastructure\Identifier;

interface AggregateRootIdentifier extends Identifier
{
}
