# KrakBoem!

I tried to extract the custom CQRS framework I initially wrote for
[dikdikdik](https://gitlab.com/rva-vzw/dikdikdik), as a dedicated PHP package,
so that I can re-use it for
[wdebelek](https://gitlab.com/rva-vzw/wdebelek).

This is very experimental, not stable at all, and probably violates
a lot of best practices. Don't say that I didn't warn you.

## It seems to work :tada: :tada: :tada: :astonished:

I managed to get this working in [dikdikdik](https://gitlab.com/rva-vzw/dikdikdik), and
it is also used in the
[wdebelek](https://gitlab.com/rva-vzw/wdebelek), card playying app.

### Install the package

If you are using symfony 6:

```
composer require rva-vzw/krakboem
```

For Symfony 5, you need version 0.10, but not that I won't maintain a Symfony 5 version.

```
composer require rva-vzw/krakboem:0.10.*
```

Now since there is no Symfony bundle (yet?) for this package, you'll
have to configure a lot of things by hand:

### Create a repository for stored events.

There is no such repository included in krakboem, you'll have to implement
`StoredEventRepository` yourself. 

You might want to take look at the `OrmStoredEventRepository` I use in *dikdikdik*:
https://gitlab.com/rva-vzw/dikdikdik/-/blob/develop/src/StoredEventRepository/OrmStoredEventRepository.php

This is an event store that uses doctrine, which is probably not ideal, but
it works. Please note that it has some hacky methods, but you only need the
ones implementing `StoredEventRepository`.

I also want to use doctrine for my read model. For easy destroying the
read model, I use a different entity manager for the event store and the
read models.

Doctrine configuration for wdebelek can be found here:
https://gitlab.com/rva-vzw/wdebelek/-/blob/develop/config/packages/doctrine.yaml

### Configure services.yaml

You'll have to configure a lot of services in services.yaml.

```
    # KrakBoem configuration

    # Event store

    RvaVzw\KrakBoem\EventSourcing\EventBus\EventStoreMiddleware:
        public: true

    RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore:
        public: true
        alias: 'RvaVzw\KrakBoem\EventSourcing\EventStore\SimpleEventStore'

    RvaVzw\KrakBoem\EventSourcing\EventStore\SimpleEventStore:
        public: true

    RvaVzw\KrakBoem\EventSourcing\EventStore\StoredEventRepository:
        alias: 'App\StoredEventRepository\OrmStoredEventRepository'

    App\StoredEventRepository\EventStreamHacks:
        public: true
        alias: 'App\StoredEventRepository\OrmStoredEventRepository'

    # Normalizers

    identifier_normalizer:
        class: RvaVzw\KrakBoem\Infrastructure\Normalizer\IdentifierNormalizer
        public: false
        tags: [serializer.normalizer]

    int_wrapper_normalizer:
        class: RvaVzw\KrakBoem\Infrastructure\Normalizer\IntWrapperNormalizer
        public: false
        tags: [serializer.normalizer]

    property_normalizer:
        class: Symfony\Component\Serializer\Normalizer\PropertyNormalizer
        public: false
        tags: [serializer.normalizer]

    # Buses

    RvaVzw\KrakBoem\EventSourcing\EventBus\MessengerEventBus:
        arguments:
            $messageBus: '@messenger.bus.events'

    RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus:
        public: true
        alias: 'RvaVzw\KrakBoem\EventSourcing\EventBus\MessengerEventBus'

    RvaVzw\KrakBoem\Cqrs\CommandBus\MessengerCommandBus:
        arguments:
            - '@messenger.bus.commands'

    RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus:
        public: true
        alias: 'RvaVzw\KrakBoem\Cqrs\CommandBus\MessengerCommandBus'

    # Enable the replay console command:
    RvaVzw\KrakBoem\EventSourcing\Replay\ReplayConsoleCommand:
        tags:
            - { name: console.command }

    RvaVzw\KrakBoem\EventSourcing\Replay\Replayer:
        arguments:
            $messageBus: '@messenger.bus.replay'

    # Listen on the correct bus
    _instanceof:
        RvaVzw\KrakBoem\EventSourcing\EventListener\EventListener:
            tags: [{ name: messenger.message_handler, bus: messenger.bus.events }]
        RvaVzw\KrakBoem\EventSourcing\Replay\Replayable:
            tags: [{ name: messenger.message_handler, bus: messenger.bus.replay }]
        RvaVzw\KrakBoem\Cqrs\CommandBus\CommandHandler:
            tags: [{ name: messenger.message_handler, bus: messenger.bus.commands }]

```

### Configure the command bus and the event bus

In `config/packages/messenger.yaml`:

```
framework:
  messenger:
    default_bus: messenger.bus.commands
    buses:
      messenger.bus.events:
        default_middleware: allow_no_handlers
        middleware:
          - 'RvaVzw\KrakBoem\EventSourcing\EventBus\EventStoreMiddleware'
      messenger.bus.commands:
      messenger.bus.replay:
        default_middleware: allow_no_handlers

    transports:
      # Uncomment the following line to enable a transport named "amqp"
      # amqp: '%env(MESSENGER_TRANSPORT_DSN)%'

    routing:
      # Route your messages to the transports
      # 'App\Message\YourMessage': amqp
```

