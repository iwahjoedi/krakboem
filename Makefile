.PHONY: qa fix php-cs-fixer phpstan psalm reset-read-model reset replay test

qa: php-cs-fixer psalm phpstan

php-cs-fixer:
	./vendor/bin/php-cs-fixer --dry-run fix .

phpstan:
	php -d memory_limit=4G ./vendor/bin/phpstan analyse --no-progress

psalm:
	./vendor/bin/psalm --no-progress --show-info=true

fix:
	./vendor/bin/php-cs-fixer fix .

# Targets below were copied from dikdikdik;
# not sure yet if I 'll keep them.

reset-read-model:
	./bin/console doctrine:schema:drop --force
	./bin/console doctrine:schema:create

# Prevent duplicate deployment with a mkdir statement.

replay:
	mkdir replay_lock
	echo MAINTENANCE=true >> .env.local
	./bin/console cache:clear
	./bin/console eventsourcing:readmodel:replay
	sed -i '/^MAINTENANCE=true$$/d' .env.local
	./bin/console cache:clear
	rmdir replay_lock

reset: reset-read-model
	./bin/console doctrine:schema:drop --force --em=event_store
	./bin/console doctrine:schema:create --em=event_store

test:
	./vendor/bin/phpunit tests
